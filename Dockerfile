FROM golang:alpine AS builder

RUN mkdir -p $GOPATH/src/hello

ADD ./hello.go $GOPATH/src/hello/hello.go

WORKDIR $GOPATH/src/hello

RUN go get -d -v

RUN go build -o /go/bin/hello

FROM scratch

COPY --from=builder /go/bin/hello /go/bin/hello

ENTRYPOINT ["/go/bin/hello"]