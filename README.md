2) Esse desafio é muito empolgante principalmente se você nunca trabalhou com a linguagem Go!
Você terá que publicar uma imagem no docker hub. Quando executarmos:

docker run <seu-user>/codeeducation

Temos que ter o seguinte resultado: Code.education Rocks!

Se você perceber, essa imagem apenas realiza um print da mensagem como resultado final, logo, vale a pena dar uma conferida no próprio site da Go Lang para aprender como fazer um "olá mundo".

Lembrando que a Go Lang possui imagens oficiais prontas, vale a pena consultar o Docker Hub.

3) A imagem de nosso projeto Go precisa ter menos de 2MB =)

Dica: No vídeo de introdução sobre o Docker quando falamos sobre o sistema de arquivos em camadas, apresento uma imagem "raiz", talvez seja uma boa utilizá-la.

Divirta-se

docker build .
docker build -t rafapaulinodev/codeeducation:latest .
docker images
docker run -i f896b1d28290


golang image: https://codefresh.io/docs/docs/learn-by-example/golang/golang-hello-world/
golang hello world: https://gobyexample.com/hello-world
outros exemplos: https://www.cloudreach.com/en/resources/blog/containerize-this-how-to-build-golang-dockerfiles/
https://github.com/crate/docker-docs/tree/master/golang

Exemplo de imagem pequena:
https://medium.com/@chemidy/create-the-smallest-and-secured-golang-docker-image-based-on-scratch-4752223b7324


Endereço da imagem:
https://hub.docker.com/repository/docker/rafapaulinodev/codeeducation

docker push rafapaulinodev/codeeducation:latest

